-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema connectpro
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema connectpro
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `connectpro` DEFAULT CHARACTER SET utf8 ;
USE `connectpro` ;

-- -----------------------------------------------------
-- Table `connectpro`.`roles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `connectpro`.`roles` (
  `role_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `desc` TEXT(300) NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`role_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `connectpro`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `connectpro`.`users` (
  `user_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` INT(10) UNSIGNED NOT NULL,
  `nric` VARCHAR(45) NOT NULL,
  `saturation` VARCHAR(45) NOT NULL,
  `name_first` VARCHAR(45) NOT NULL,
  `name_last` VARCHAR(45) NOT NULL,
  `address` VARCHAR(255) NULL,
  `tel_mobile` INT NULL,
  `tel_home` INT NULL,
  `dob` DATE NULL,
  `gender` ENUM('M', 'F') NOT NULL,
  `dialect` VARCHAR(45) NULL,
  `lang_cnt` INT NULL,
  `skillset_cnt` INT NULL,
  `interest_cnt` INT NULL,
  `img_filename` VARCHAR(80) NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `fk_member_profiles_role1`
    FOREIGN KEY (`role_id`)
    REFERENCES `connectpro`.`roles` (`role_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_member_profiles_role1_idx` ON `connectpro`.`users` (`role_id` ASC);


-- -----------------------------------------------------
-- Table `connectpro`.`emails`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `connectpro`.`emails` (
  `email_id` VARCHAR(80) NOT NULL,
  `user_id` INT(10) UNSIGNED NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`email_id`),
  CONSTRAINT `fk_users_user_profiles1`
    FOREIGN KEY (`user_id`)
    REFERENCES `connectpro`.`users` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_users_user_profiles1_idx` ON `connectpro`.`emails` (`user_id` ASC);


-- -----------------------------------------------------
-- Table `connectpro`.`next_of _kins`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `connectpro`.`next_of _kins` (
  `user_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name_first` VARCHAR(45) NOT NULL,
  `name_last` VARCHAR(45) NOT NULL,
  `nric` VARCHAR(45) NOT NULL,
  `relationship` VARCHAR(45) NOT NULL,
  `tel` INT NOT NULL,
  `email` VARCHAR(45) NULL,
  `address` VARCHAR(255) NULL,
  `clan` VARCHAR(45) NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `fk_NOK_user_profiles1`
    FOREIGN KEY (`user_id`)
    REFERENCES `connectpro`.`users` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_NOK_user_profiles1_idx` ON `connectpro`.`next_of _kins` (`user_id` ASC);


-- -----------------------------------------------------
-- Table `connectpro`.`organisation`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `connectpro`.`organisation` (
  `organisation_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(80) NULL,
  `description` TINYTEXT NULL,
  `img_filename` VARCHAR(80) NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`organisation_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `connectpro`.`events`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `connectpro`.`events` (
  `event_id` INT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `organisation_id` INT(10) UNSIGNED NOT NULL,
  `brief_desc` TINYTEXT NOT NULL,
  `detail_desc` TEXT(10000) NOT NULL,
  `date` DATE NOT NULL,
  `start_time` TIME NOT NULL,
  `end_time` TIME NOT NULL,
  `venue` VARCHAR(80) NOT NULL,
  `category` VARCHAR(45) NOT NULL,
  `direction` TINYTEXT NULL,
  `note` TEXT(500) NULL,
  `member_age_max` INT NULL,
  `member_age_min` INT NULL,
  `meal_avail` TINYINT NULL,
  `shirt_avail` TINYINT NULL,
  `img_filename` VARCHAR(80) NULL,
  `map_location` VARCHAR(80) NULL,
  `approved` TINYINT NULL,
  `approved_user_id` INT UNSIGNED NULL,
  `published` TINYINT NULL,
  `published_user_id` INT UNSIGNED NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`event_id`),
  CONSTRAINT `fk_events_organisation1`
    FOREIGN KEY (`organisation_id`)
    REFERENCES `connectpro`.`organisation` (`organisation_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE UNIQUE INDEX `eventId_UNIQUE` ON `connectpro`.`events` (`event_id` ASC);

CREATE INDEX `fk_events_organisation1_idx` ON `connectpro`.`events` (`organisation_id` ASC);


-- -----------------------------------------------------
-- Table `connectpro`.`languages`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `connectpro`.`languages` (
  `lang_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `languages` VARCHAR(45) NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`lang_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `connectpro`.`users_languages`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `connectpro`.`users_languages` (
  `user_lang_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT(10) UNSIGNED NOT NULL,
  `lang_id` INT(10) UNSIGNED NOT NULL,
  `idx` INT(4) UNSIGNED NOT NULL,
  `lang` VARCHAR(45) NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_lang_id`),
  CONSTRAINT `fk_languages_member_profiles1`
    FOREIGN KEY (`user_id`)
    REFERENCES `connectpro`.`users` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_languages_languages1`
    FOREIGN KEY (`lang_id`)
    REFERENCES `connectpro`.`languages` (`lang_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_languages_member_profiles1_idx` ON `connectpro`.`users_languages` (`user_id` ASC);

CREATE UNIQUE INDEX `userId_UNIQUE` ON `connectpro`.`users_languages` (`user_id` ASC);

CREATE INDEX `fk_users_languages_languages1_idx` ON `connectpro`.`users_languages` (`lang_id` ASC);

CREATE UNIQUE INDEX `languages_lang_id_UNIQUE` ON `connectpro`.`users_languages` (`lang_id` ASC);


-- -----------------------------------------------------
-- Table `connectpro`.`skillsets`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `connectpro`.`skillsets` (
  `skillsets_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `skillset` VARCHAR(45) NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`skillsets_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `connectpro`.`user_skillsets`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `connectpro`.`user_skillsets` (
  `user_skillset_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT(10) UNSIGNED NOT NULL,
  `skillsets_id` INT UNSIGNED NOT NULL,
  `idx` INT(10) UNSIGNED NOT NULL,
  `skillset` VARCHAR(45) NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_skillset_id`),
  CONSTRAINT `fk_skillsets_member_profiles1`
    FOREIGN KEY (`user_id`)
    REFERENCES `connectpro`.`users` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_skillsets_skillsets1`
    FOREIGN KEY (`skillsets_id`)
    REFERENCES `connectpro`.`skillsets` (`skillsets_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_skillsets_member_profiles1_idx` ON `connectpro`.`user_skillsets` (`user_id` ASC);

CREATE INDEX `fk_user_skillsets_skillsets1_idx` ON `connectpro`.`user_skillsets` (`skillsets_id` ASC);

CREATE UNIQUE INDEX `user_id_UNIQUE` ON `connectpro`.`user_skillsets` (`user_id` ASC);

CREATE UNIQUE INDEX `skillsets_id_UNIQUE` ON `connectpro`.`user_skillsets` (`skillsets_id` ASC);


-- -----------------------------------------------------
-- Table `connectpro`.`causes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `connectpro`.`causes` (
  `cause_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cause` VARCHAR(45) NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`cause_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `connectpro`.`user_causes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `connectpro`.`user_causes` (
  `user_cause_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT(10) UNSIGNED NOT NULL,
  `cause_id` INT(10) UNSIGNED NOT NULL,
  `idx` INT(10) UNSIGNED NOT NULL,
  `cause` VARCHAR(45) NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_cause_id`),
  CONSTRAINT `fk_interests_member_profiles1`
    FOREIGN KEY (`user_id`)
    REFERENCES `connectpro`.`users` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_interests_causes1`
    FOREIGN KEY (`cause_id`)
    REFERENCES `connectpro`.`causes` (`cause_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_interests_member_profiles1_idx` ON `connectpro`.`user_causes` (`user_id` ASC);

CREATE INDEX `fk_interests_causes1_idx` ON `connectpro`.`user_causes` (`cause_id` ASC);

CREATE UNIQUE INDEX `user_id_UNIQUE` ON `connectpro`.`user_causes` (`user_id` ASC);

CREATE UNIQUE INDEX `causes_id_UNIQUE` ON `connectpro`.`user_causes` (`cause_id` ASC);


-- -----------------------------------------------------
-- Table `connectpro`.`tasks`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `connectpro`.`tasks` (
  `task_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `desc` TEXT(300) NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`task_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `connectpro`.`event_users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `connectpro`.`event_users` (
  `event_user_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `event_id` INT(10) UNSIGNED NOT NULL,
  `user_id` INT(10) UNSIGNED NOT NULL,
  `role_id` INT(10) UNSIGNED NOT NULL,
  `task_id` INT(10) UNSIGNED NOT NULL,
  `dietary` VARCHAR(80) NULL,
  `shirt_size` VARCHAR(45) NULL,
  `interested_role_id` VARCHAR(45) NULL,
  `remark` TEXT(500) NULL,
  `attendance` ENUM('P', 'A', 'C') NULL,
  `performance` VARCHAR(80) NULL,
  `rating` INT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`event_user_id`),
  CONSTRAINT `fk_events1`
    FOREIGN KEY (`event_id`)
    REFERENCES `connectpro`.`events` (`event_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_member_profiles1`
    FOREIGN KEY (`user_id`)
    REFERENCES `connectpro`.`users` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_memberEvents_role1`
    FOREIGN KEY (`role_id`)
    REFERENCES `connectpro`.`roles` (`role_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_event_users_tasks1`
    FOREIGN KEY (`task_id`)
    REFERENCES `connectpro`.`tasks` (`task_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_memberUpcomingEvents_events1_idx` ON `connectpro`.`event_users` (`user_id` ASC);

CREATE INDEX `fk_memberUpcomingEvents_member_profiles1_idx` ON `connectpro`.`event_users` (`event_id` ASC);

CREATE UNIQUE INDEX `eventId_UNIQUE` ON `connectpro`.`event_users` (`event_id` ASC);

CREATE UNIQUE INDEX `userId_UNIQUE` ON `connectpro`.`event_users` (`user_id` ASC);

CREATE INDEX `fk_membeEvents_role1_idx` ON `connectpro`.`event_users` (`role_id` ASC);

CREATE INDEX `fk_event_users_tasks1_idx` ON `connectpro`.`event_users` (`task_id` ASC);


-- -----------------------------------------------------
-- Table `connectpro`.`access_rights`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `connectpro`.`access_rights` (
  `access_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `permission` VARCHAR(45) NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`access_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `connectpro`.`role_access_right`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `connectpro`.`role_access_right` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` INT(10) UNSIGNED NOT NULL,
  `access_id` INT(10) UNSIGNED NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_access_right_role1`
    FOREIGN KEY (`role_id`)
    REFERENCES `connectpro`.`roles` (`role_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_role_access_right_access_rights1`
    FOREIGN KEY (`access_id`)
    REFERENCES `connectpro`.`access_rights` (`access_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_access_right_role1_idx` ON `connectpro`.`role_access_right` (`role_id` ASC);

CREATE INDEX `fk_role_access_right_access_rights1_idx` ON `connectpro`.`role_access_right` (`access_id` ASC);

CREATE UNIQUE INDEX `roleId_UNIQUE` ON `connectpro`.`role_access_right` (`role_id` ASC);

CREATE UNIQUE INDEX `accessId_UNIQUE` ON `connectpro`.`role_access_right` (`access_id` ASC);


-- -----------------------------------------------------
-- Table `connectpro`.`badges`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `connectpro`.`badges` (
  `badge_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `desc` TEXT(300) NULL,
  `img_filename` VARCHAR(80) NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`badge_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `connectpro`.`user_badges`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `connectpro`.`user_badges` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT(10) UNSIGNED NOT NULL,
  `badge_id` INT(10) UNSIGNED NOT NULL,
  `date_aquired` DATE NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_member_profiles_has_badges_member_profiles1`
    FOREIGN KEY (`user_id`)
    REFERENCES `connectpro`.`users` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_member_profiles_has_badges_badges1`
    FOREIGN KEY (`badge_id`)
    REFERENCES `connectpro`.`badges` (`badge_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_member_profiles_has_badges_badges1_idx` ON `connectpro`.`user_badges` (`badge_id` ASC);

CREATE INDEX `fk_member_profiles_has_badges_member_profiles1_idx` ON `connectpro`.`user_badges` (`user_id` ASC);

CREATE UNIQUE INDEX `member_profiles_userId_UNIQUE` ON `connectpro`.`user_badges` (`user_id` ASC);

CREATE UNIQUE INDEX `badges_badgeId_UNIQUE` ON `connectpro`.`user_badges` (`badge_id` ASC);


-- -----------------------------------------------------
-- Table `connectpro`.`alerts`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `connectpro`.`alerts` (
  `alert_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `alert_msg` TEXT(300) NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`alert_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `connectpro`.`user_alerts`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `connectpro`.`user_alerts` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT(10) UNSIGNED NOT NULL,
  `event_id` INT(10) UNSIGNED NOT NULL,
  `alert_id` INT(10) UNSIGNED NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_alerts_member_profiles1`
    FOREIGN KEY (`user_id`)
    REFERENCES `connectpro`.`users` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_alerts_events1`
    FOREIGN KEY (`event_id`)
    REFERENCES `connectpro`.`events` (`event_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_alerts_alerts1`
    FOREIGN KEY (`alert_id`)
    REFERENCES `connectpro`.`alerts` (`alert_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_alerts_member_profiles1_idx` ON `connectpro`.`user_alerts` (`user_id` ASC);

CREATE INDEX `fk_alerts_events1_idx` ON `connectpro`.`user_alerts` (`event_id` ASC);

CREATE INDEX `fk_user_alerts_alerts1_idx` ON `connectpro`.`user_alerts` (`alert_id` ASC);

CREATE UNIQUE INDEX `user_id_UNIQUE` ON `connectpro`.`user_alerts` (`user_id` ASC);

CREATE UNIQUE INDEX `event_id_UNIQUE` ON `connectpro`.`user_alerts` (`event_id` ASC);


-- -----------------------------------------------------
-- Table `connectpro`.`rules`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `connectpro`.`rules` (
  `rule_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `rule` INT NOT NULL,
  `is_greater` TINYINT NOT NULL,
  `is_lesser` TINYINT NOT NULL,
  `is_greaterthan_equal` TINYINT NOT NULL,
  `is_lessthan_equal` TINYINT NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`rule_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `connectpro`.`badge_rules`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `connectpro`.`badge_rules` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `rule_id` INT(10) UNSIGNED NOT NULL,
  `badge_id` INT(10) UNSIGNED NOT NULL,
  `desc` VARCHAR(80) NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_badge_rules_badges1`
    FOREIGN KEY (`badge_id`)
    REFERENCES `connectpro`.`badges` (`badge_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_badge_rules_rules1`
    FOREIGN KEY (`rule_id`)
    REFERENCES `connectpro`.`rules` (`rule_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_badge_rules_badges1_idx` ON `connectpro`.`badge_rules` (`badge_id` ASC);

CREATE UNIQUE INDEX `badge_id_UNIQUE` ON `connectpro`.`badge_rules` (`badge_id` ASC);

CREATE INDEX `fk_badge_rules_rules1_idx` ON `connectpro`.`badge_rules` (`rule_id` ASC);

CREATE UNIQUE INDEX `rule_id_UNIQUE` ON `connectpro`.`badge_rules` (`rule_id` ASC);


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
